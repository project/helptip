
DESCRIPTION
-----------

Introduces a new node type called "help tip".  Also blocks for
displaying help tips.  Each tip is configured to appear only on one
or more pages, allowing it to be context sensitive.

Blocks may show one help tip or, if more than one help tip applies
to a given page, a block may show several.

If so configured, a user may hide a tip they do not wish to see it
again.

CREDITS
-------

Dave Cohen <http://drupal.org/user/18468>

Inspired by Helpedit <http://drupal.org/node/18031> by Aran Deltac <http://drupal.org/user/16725>

